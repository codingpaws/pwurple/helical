## Helical

Helical is a wrapper on top of [nicklaw5/helix/v2][helix]. It
automatically turns HTTP errors into Go errors, making it more ergonomic
to work with.

[helix]: https://github.com/nicklaw5/helix

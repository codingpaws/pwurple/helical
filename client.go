package helical

import (
	"context"

	"github.com/nicklaw5/helix/v2"
)

type client struct {
	helix *helix.Client
}

func NewClient(options *helix.Options) (Client, error) {
	return NewClientWithContext(context.Background(), options)
}

func NewClientWithContext(ctx context.Context, options *helix.Options) (Client, error) {
	helix, err := helix.NewClientWithContext(ctx, options)
	if err != nil {
		return nil, err
	}
	return &client{helix}, nil
}

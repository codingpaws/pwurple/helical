module gitlab.com/codingpaws/pwurple/helical

go 1.21.7

require github.com/nicklaw5/helix/v2 v2.28.3

require github.com/golang-jwt/jwt/v4 v4.0.0 // indirect
